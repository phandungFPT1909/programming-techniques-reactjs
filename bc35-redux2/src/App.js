import logo from './logo.svg';
import './App.css';
import Ex_shoe_shoe_redux from './Ex_shoe_shop_redux/Ex_shoe_shoe_redux';

function App() {
  return (
    <div className="App">
      <Ex_shoe_shoe_redux />
    </div>
  );
}

export default App;
