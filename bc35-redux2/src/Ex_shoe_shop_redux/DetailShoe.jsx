
import React, { Component } from 'react'
import { connect } from 'react-redux'

 class DetailShoe extends Component {
  render() {
    let {image, id, name, description, price} = this.props.detail
    return (
      <div className='row mt-5 alert-secondary p-5 text-left'>
        <img src={image} alt="" className='col-3'/>
        <div className='col-9'>
            <p>ID: {id}</p>
            <p>Name: {name}</p>
            <p>Desc: {description}</p>
            <p>Price: {price}</p>
        </div>
      </div>
    )
  }
}

let mapStateToProps = (state) => {
    return {
        detail: state.shoeReducer_ShoeShop.detail
    }
}
export default connect(mapStateToProps,null)(DetailShoe)
// XEM_CHI_TIET