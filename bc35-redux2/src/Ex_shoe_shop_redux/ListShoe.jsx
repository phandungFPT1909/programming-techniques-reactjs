import React, { Component } from 'react'
import { dataShoe } from './dataShoe'
import ItemShoe from './ItemShoe'
export default class ListShoe extends Component {
    renderListShoe = () => {
        return dataShoe.map((itemShoe,index) => {
            return <ItemShoe itemShoe={itemShoe} key={index}/>
        })
    }
  render() {
    return (
      <div className='row'>
        {this.renderListShoe()}
      </div>
    )
  }
}
