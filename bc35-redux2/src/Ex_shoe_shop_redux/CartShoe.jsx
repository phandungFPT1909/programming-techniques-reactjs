import React, { Component } from 'react'
import { connect } from 'react-redux';
import styles from "./styles.module.css"

 class CartShoe extends Component {
    renderTbody = () => {
        return this.props.gioHang?.map((item) => { 
            return <tr>
                <td>{item.id}</td>
                <td>{item.name}</td>
                <td><img src={item.image} style={{width:"50px"}} alt="" /></td>
                <td>{item.price}</td>
                <td>
                    <button onClick={() => {
                        this.props.tangGiamSoLuong(item.id,false)
                    }} className='btn btn-primary px-3 mr-1'>-</button>
                    {item.soLuong}
                    <button onClick={() => {
                        this.props.tangGiamSoLuong(item.id,true)
                    }} className='btn btn-primary px-3 ml-1'>+</button>
                 </td>
                <td>{item.price * item.soLuong}</td>
                <td>
                    <button onClick={() => {
                        this.props.clearItemCart(item.id)
                    }} className='btn btn-success'>Xóa</button>
                </td>
            </tr>
        })
    }

  render() {
    let tongTien = this.props.gioHang.reduce((totalMoney,item,index) => {
        return totalMoney += item.soLuong * item.price
    },0)
    return (
        <table className='table'>
            <thead>
                <tr>
                    <th>id</th>
                    <th>name</th>
                    <th>image</th>
                    <th>price</th>
                    <th>quantity</th>
                    <th>totalMoney</th>
                    <th>Manipulation</th>
                </tr>
            </thead>
            <tbody>
                {this.renderTbody()}
            </tbody>
            <tfoot>
                <th colSpan={5}></th>
                <th>Tổng tiền</th>
                <th>{tongTien}</th>
            </tfoot>
        </table>
    )
  }
}

const mapStateToProps = (state) => {
    return {
        gioHang: state.shoeReducer_ShoeShop.cart
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        clearItemCart:(idSp) => {
            const action = {
                type:"XOA_ITEM_CART",
                idSp
            }
            dispatch(action)
        },
        tangGiamSoLuong:(idSp,tangGiam) => {
            const action = {
                type:"TANG_GIAM_SO_LUONG",
                idSp,
                tangGiam
            }
            dispatch(action)
        }
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(CartShoe)
