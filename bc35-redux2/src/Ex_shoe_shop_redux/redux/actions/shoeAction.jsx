// action creator

import { THEM_GIO_HANG, XEM_CHI_TIET } from "../constants/shoeContant"


export const chanDetailAction = (value) => {
    return {
        type: XEM_CHI_TIET,
        payload:value,
    }
}

export const chanAddToCartAction = (value) => {
    return {
        type: THEM_GIO_HANG,
        payload:value,
    }
}