import { combineReducers } from "redux";
import { shoeReducer_ShoeShop } from './shoeShopReducer';


// Store tổng ứng dụng
export const rootReducer = combineReducers({
    shoeReducer_ShoeShop,
})